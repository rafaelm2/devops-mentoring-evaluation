import React, { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { Button, VStack, Heading, Text, Flex, Box } from "@chakra-ui/react";
import { QuizContext } from "../Context/QuizContextProvider";

function Quiz() {
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [timeRemaining, setTimeRemaining] = useState(60);
  const [score, setScore] = useState(0);
  const [quizData, setQuizData] = useState([
    {
      question: "¿Qué es Docker?",
      options: ["Una plataforma de virtualización", "Un sistema operativo", "Una herramienta de contenedores", "Un lenguaje de programación"],
      answer: "Una herramienta de contenedores",
    },
    {
      question: "¿Cuál de las siguientes afirmaciones sobre los contenedores Docker es correcta?",
      options: ["Los contenedores Docker son más lentos que las máquinas virtuales", "Los contenedores Docker requieren un hipervisor para funcionar", "Los contenedores Docker son ligeros y comparten el núcleo del sistema operativo del host", "Los contenedores Docker solo funcionan en entornos de desarrollo"],
      answer: "Los contenedores Docker son ligeros y comparten el núcleo del sistema operativo del host",
    },
    {
      question: "¿Cuál es el comando de Docker utilizado para construir una imagen a partir de un archivo Dockerfile?",
      options: ["docker run", "docker pull", "docker build", "docker create"],
      answer: "docker build",
    },
    {
      question: "¿Cuál es el propósito del archivo Docker Compose?",
      options: ["Orquestar múltiples contenedores Docker en un entorno de producción", "Ejecutar un solo contenedor Docker en un entorno de desarrollo", "Definir las variables de entorno para un contenedor Docker", "Descargar imágenes de Docker Hub"],
      answer: "Orquestar múltiples contenedores Docker en un entorno de producción",
    },
    {
      question: "¿Cuál es la diferencia entre una imagen de Docker y un contenedor de Docker?",
      options: ["Una imagen es una instancia en ejecución de un contenedor", "Una imagen es un archivo de configuración, mientras que un contenedor es una instancia en ejecución de esa imagen", "Una imagen es un paquete ligero que contiene todo lo necesario para ejecutar una pieza de software, mientras que un contenedor es una instancia en ejecución de esa imagen", "No hay diferencia, los términos 'imagen' y 'contenedor' se usan indistintamente"],
      answer: "Una imagen es un paquete ligero que contiene todo lo necesario para ejecutar una pieza de software, mientras que un contenedor es una instancia en ejecución de esa imagen",
    },
    {
      question: "¿Cuál es la función principal de GitLab?",
      options: ["Gestión de versiones de código fuente", "Gestión de proyectos y colaboración", "Orquestación de contenedores", "Protección de datos sensibles"],
      answer: "Gestión de proyectos y colaboración",
      },
      {
      question: "¿Cuál es la diferencia entre un repositorio público y un repositorio privado en GitLab?",
      options: ["Un repositorio público es visible y accesible para todos, mientras que un repositorio privado requiere permisos para acceder", "Un repositorio público se puede clonar y modificar, mientras que un repositorio privado solo permite la visualización del código fuente", "No hay diferencia, ambos tipos de repositorios son idénticos", "Un repositorio público tiene un almacenamiento limitado, mientras que un repositorio privado ofrece almacenamiento ilimitado"],
      answer: "Un repositorio público es visible y accesible para todos, mientras que un repositorio privado requiere permisos para acceder",
      },
      {
      question: "¿Qué es un GitLab Pipeline?",
      options: ["Una herramienta para el control de versiones de código fuente", "Un entorno de desarrollo local", "Una secuencia de pasos automatizados para construir, probar y desplegar aplicaciones", "Una técnica para administrar el control de cambios en Git"],
      answer: "Una secuencia de pasos automatizados para construir, probar y desplegar aplicaciones",
      },
      {
      question: "¿Cuál es el archivo de configuración utilizado para definir un GitLab Pipeline?",
      options: [".gitignore", "Dockerfile", "package.json", ".gitlab-ci.yml"],
      answer: ".gitlab-ci.yml",
      },
      {
      question: "¿Qué significa AWS?",
      options: ["Amazon Web Services", "Advanced Web Security", "Application Workflow System", "Automated Web Solutions"],
      answer: "Amazon Web Services",
      }
  ]);
  const navigate = useNavigate();
  const { QuizResult, setQuizResult } = useContext(QuizContext)
  useEffect(() => {
    const timer =
      timeRemaining > 0 &&
      setInterval(() => {
        setTimeRemaining((prev) => prev - 1);
      }, 1000);
    return () => clearInterval(timer);
  }, [timeRemaining]);

  useEffect(() => {
    const timer =
      timeRemaining === 0 &&
      setTimeout(() => {
        const nextQuestion = currentQuestion + 1;
        if (nextQuestion < quizData.length) {
          setCurrentQuestion(nextQuestion);
          setTimeRemaining(60);
        } else {
          setQuizResult(score)
          setTimeout(() => {
            navigate("/result", { score: score });
          }, 100);

        }
      }, 1000);
    return () => clearTimeout(timer);
  }, [timeRemaining, currentQuestion]);
  const handleAnswerOptionClick = (isCorrect) => {
    if (isCorrect) {
      setScore(score + 1);
    }

    const nextQuestion = currentQuestion + 1;
    if (nextQuestion < quizData.length) {
      setCurrentQuestion(nextQuestion);
      setTimeRemaining(60);
    } else {
      setQuizResult(score)
      setTimeout(() => {
        navigate("/result", { score: score });
      }, 100);

    }
  };
  setQuizResult(score)
  
  return (
    <VStack fontFamily={'Lora'} pt={'20px'} bg={'#eff1f7'} h={'100vh'}>
      <Flex width={'90%'} color={'white'} justifyContent={'space-between'} fontSize={'12px'} fontWeight={500}>
        <Box borderRadius={'10px'} p={'2px 15px'} bg={'green'}>0{currentQuestion + 1}/10</Box>
        <Box borderRadius={'10px'} p={'2px 15px'} bg={'#b483d7'}> {timeRemaining} </Box>
      </Flex>
      <Text p={'5px'} pt={'50px'} fontSize="17px" textAlign="center">
        {quizData[currentQuestion].question}
      </Text>

      <VStack pt={'50px'}>
        {quizData[currentQuestion].options.map((option) => (
          <Box
            key={option}
            onClick={() =>
              handleAnswerOptionClick(option === quizData[currentQuestion].answer)
            }
            p={'15px 90px'}
            boxShadow={'xl'}
            width={'100%'}
          
            borderRadius={'10px'}
            mt={'80px'}
            bg={'#2085a8'}
            color={'white'}
            textAlign={'center'}
            cursor={'pointer'}
          >
            {option}
          </Box>
        ))}
      </VStack>
    </VStack>
  );
}

export default Quiz;
